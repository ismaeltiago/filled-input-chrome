**How the chrome extension works:**

![image-gif](http://g.recordit.co/i26AZxxBdQ.gif)

**Fill Input Chrome** is a Chrome extension to fill the form inputs like as CPF, CNPJ and others.

See [How to add unpacked extension in the Chrome?](https://gitlab.com/ismaeltiago/filled-input-chrome/wikis/How-to-add-unpacked-extension-in-the-Chrome%3F)
